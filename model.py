import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, recall_score
from sklearn.model_selection import train_test_split


df = pd.read_csv('training_data.csv')
X= pd.DataFrame(df, columns=['Acc','Gyro','Prox','Rot'])
Y= pd.DataFrame(df, columns=['Label'])


x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.25)


log_model = LogisticRegression(multi_class="multinomial", solver="newton-cg", max_iter=1000)
log_model.fit(x_train,y_train)


pred_test = log_model.predict(x_test)
acu = accuracy_score(y_test, pred_test)
recall = recall_score(y_test, pred_test, average="macro")

print(acu)
print(recall)

# Save your model
import joblib
joblib.dump(log_model, 'model.pkl')
print("Model dumped!")

# Load the model that you just saved
lr = joblib.load('model.pkl')

# Saving the data columns from training
model_columns = list(X.columns)
joblib.dump(model_columns, 'model_columns.pkl')
print("Models columns dumped!")